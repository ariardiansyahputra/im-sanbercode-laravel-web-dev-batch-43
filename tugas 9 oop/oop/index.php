<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "nama : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs ."<br>" ; // 4
echo "cold_blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$sungokong = new ape("kera sakti");
echo "nama : " . $sungokong->name . "<br>"; // "shaun"
echo "legs : " . $sungokong->legs ."<br>" ; // 4
echo "cold_blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell() ."<br><br>";

$kodok = new Frog("buduk");
echo "nama : " . $kodok->name . "<br>"; // "shaun"
echo "legs : " . $kodok->legs ."<br>" ; // 4
echo "cold_blooded : " . $kodok->cold_blooded . "<br>";
echo "Yell : " . $kodok->jump() ."<br>";

?>
