<!doctype html>
<html>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="\welcome" method="post">
            @csrf
            <label>First Name</label><br><br>
            <input type="text" name="fname"><br><br>
            <label>Last Name</label><br><br>
            <input type="text" name="lname"><br><br>
            <label>Gender</label><br><br>
            <input type="radio">Male<br>
            <input type="radio">Female<br>
            <input type="radio">Other<br><br>
            <label>Nationality</label><br><br>
            <Select name="nationality"><br>
                <option value="Indonesian">Indonesian</option>
                <option value="Australia">Australia</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
            </Select> <br><br>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
            <input type="checkbox" name="Language Spoken">English<br>
            <input type="checkbox" name="Language Spoken">Other<br><br>
            <label>Bio</label><br>
            <textarea name="Bio" rows="10" cols="30"></textarea>
            <br><br>
            <input type="submit" name="Sign Up">
        </form>
    </body>
</html>